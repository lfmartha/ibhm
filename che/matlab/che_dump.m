%% che_dump.m
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% For a <che.html *che*> mesh object in memory, this function prints adjacency
% information based on cells, edges, and vertices, and prints list of mesh
% boundary edge ids.
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
function [] = che_dump(m)
%% Mesh adjacency information based on cells
fprintf(1,'\n');
fprintf(1,'Edges and vertices of cells:\n');
for c_id = 1:m.nc
    [edges,vertices] = get_ev_c(m, c_id);
    nvertices = size(vertices,2);
    fprintf(1,'Face: %2d\n', c_id);
    for k = 1:nvertices
        fprintf(1,'e%d: %2d\t', k, edges(k));
    end
    fprintf(1,'\n');
    for k = 1:nvertices
        fprintf(1,'v%d: %2d\t', k, vertices(k));
    end
    fprintf(1,'\n');
end
fprintf(1,'\n');

%% Mesh adjacency information based on edges
fprintf(1,'Cells and vertices of edges:\n');
for e_id = 1:m.ne
    [cells,vertices] = get_cv_e(m, e_id);
    ncells = size(cells,2);
    fprintf(1,'Edge: %2d\n', e_id);
    for k = 1:ncells
        fprintf(1,'f%d: %2d\t', k, cells(k));
    end
    fprintf(1,'\n');
    fprintf(1,'v1: %2d\tv2: %2d\n', vertices(1), vertices(2));
    fprintf(1,'\n');
end
fprintf(1,'\n');

%% Mesh adjacency information based on vertices
fprintf(1,'Star cells and edges of vertices:\n');
for v_id = 1:m.nv
    [cells, edges] = get_ce_v(m, v_id);
    ncells = size(cells,2);
    nedges = size(edges,2);
    fprintf(1,'Vertex: %2d\n', v_id);
    for k = 1:ncells
        fprintf(1,'f%d: %2d\t', k, cells(k));
    end
    fprintf(1,'\n');
    for k = 1:nedges
        fprintf(1,'e%d: %2d\t', k, edges(k));
    end
    fprintf(1,'\n');
end
fprintf(1,'\n');

%% Mesh boundary information
fprintf(1,'Boundary curves (connected portions of boundary) and boundary edges:\n');
for b_id = 1:m.nbdry
    edges = get_e_bdry(m, b_id);
    nedges = size(edges,2);
    fprintf(1,'Boundary curve: %2d\n', b_id);
    for k = 1:nedges
        fprintf(1,'e%d: %2d\t', k, edges(k));
    end
    fprintf(1,'\n');
    fprintf(1,'\n');
end
end

