%% run_che_test.m
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% Using <che_test.html *che_test*>, this code creates a simple mesh (with
% five triangles and one quadrilateral), prints adjacency information
% based on cells, edges, and vertices, and prints list of mesh boundary
% edge ids.
%
% Mesh description in file <che_test-example.pdf>
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Mesh creation using <che_test.html *che_test*> with 8 vertices and 6 faces
clear
clc

m = che_test(8,6);             % using constructor with arguments

nv_c = 3;                      % loading first triangle
m.load_cell(1,nv_c,[1 4 3]); 
nv_c = 4;                      % loading first quadrangle
m.load_cell(2,nv_c,[7 8 2 1]);
nv_c = 3;                      % loading a list of triangles 
m.load_cell(3,nv_c,[2 4 1]);
m.load_cell(4,nv_c,[2 5 4]);
m.load_cell(5,nv_c,[2 6 5]);
m.load_cell(6,nv_c,[8 6 2]);

%% Construction of <che.html *che*> mesh resprentation
build(m);

%% Print mesh adjacency information using function <che_dump.html *che_dump*>
che_dump(m);

%% Clean-up memory
clean(m);
delete(m);
