%% CHE_GMSH subclass of <che.html *che*> class for a mesh defined in a gmsh file.
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
%
% This is a subclass of <che.html *che*> class to demonstrate its usage
% creating a 2D mesh from a gmsh file.
%
% Please, refer to documentation of <che.html *che*> class.
%
% <che.html *che*> is an abstract class, which contains abstract methods:  
%%%
% * *get_nv_cell*, which returns the number of vertices of a cell.
% * *get_v_cell*, which returns a vertex of a cel.
% 
% This subclass implements these two abstract methods. Therefore, it is
% a concrete class that can be instatiated (i.e., it is possible to create
% objects of this subclass).
%
% Using this instantiation the user can look at the data in the gmsh
% file from two viewpoints:
%%%
% * as defined in gmsh (the elements)
% * as a set of topological entities of decreasing dimension
%   (volumes, faces, edges, vertices)
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Class definition
classdef che_gmsh < che

    %% Public properties
    properties
        %
        % From the mesh, related to the elements
        %
        n_elem       % Number of elements (as defined in gmsh, i.e. n_elem may 
                     %                     include nodes, lines and volumes)
        n_elem_by_dimension      % 
        ele_infos    % Information about the elements: 1 - element number, 2 - type, 3 - ntags
        ele_nodes    % The nodes of the elements
        ele_tags     % The tags assigned to the element
        elem_of_number %
        %
        % From the mesh, related to the nodes
        %
        n_nodes      % Number of nodes
        coords       % The coordinates of the nodes
        number_of_node % The number assigned to each node
        node_of_number
        %
        % For the interaction with the topology in CHE
        %
        nodes_verts  % The mapping from vertex number to node number
        verts_nodes
        elems_faces
        faces_elems
        elems_edges
        edges_elems
        elems_verts
        verts_elems
        %
        % Specific to the class
        %
        types   % Characteristics (0D/1D/2D/3D, nnodes, nverts) 
                % of each type of element defined in gmsh 
                % Should be possible to automatically update from the source!
                % Could be static?
    end

    %% Constructor method
    methods
        %-----------------------------------------------------------------------
        % Constructor method.
        function obj = che_gmsh
            obj = obj@che;        % using constructor without arguments
            obj.types = [ ...
                1 2 2 ; ... % Lines L2     % 1 
                2 3 3 ; ... % Tris T3
                2 4 4 ; ... % Quads Q4
                3 4 4 ; ... % Tets Tet4
                3 8 8 ; ... % Hexas H8     % 5
                3 6 6 ; ... % Prism Pr6
                3 5 5 ; ... % Pyramid Py5
                1 3 2 ; ... % Lines L3
                2 6 3 ; ... % Tris T6
                2 8 4 ; ... % Quads Q9     % 10
                3 10 4 ; ... % Tets T10
                3 27 8 ; ... % Hexas H27
                3 18 6 ; ... % Prism Pr18
                3 14 5 ; ... % Pyramid 14
                0 1 1 ; ... % Points       % 15
                2 8 4 ; ... Quads Q8 
                3 20 8 ; ... Hexas H20
                3 15 6 ; ... Prism Pr15
                3 13 5 ]; ... Pyramid 13
        end
    end
        
    %% Public method to load gmsh data from a file
    methods
        %-----------------------------------------------------------------------
        % Load data from a file
        function load(obj, name)
            % Inspired in load_gmsh2.m by JP Moitinho de Almeida (moitinho@civil.ist.utl.pt)
            % and  R Lorphevre(r(point)lorphevre(at)ulg(point)ac(point)be)
            
            % Clean up the existing object
            obj.clean();
                     
            fid = fopen(name, 'r');
            
            %-------------------------------------------------------------------
            % Identify file format
            tline = fgetl(fid);
            if (feof(fid))
                fprintf('Empty file: %s',  filename);
                % In case of error the clean object is returned
                return;
            end
            if (strcmp(tline, '$NOD'))
                fileformat = 1;
            else
                if (strcmp(tline, '$MeshFormat'))
                    fileformat = 2;
                    tline = fgetl(fid);
                    if (feof(fid))
                        fprintf('Syntax error (no version) in: %s',  filename);
                        fileformat = 0;
                    end
                    [ form ] = sscanf( tline, '%f %d %d');
                    if (form(1) ~= 2 && (form(1) ~= 2.1 && form(1) ~= 2.2))
                        fprintf('Unknown mesh format: %s', tline);
                        fileformat = 0;
                    end
                    if (form(2) ~= 0)
                        fprintf('Sorry, this program can only read ASCII format');
                        fileformat = 0;
                    end
                    fgetl(fid);    % this should be $EndMeshFormat
                    if (feof(fid))
                        fprintf('Syntax error (no $EndMeshFormat) in: %s',  filename);
                        fileformat = 0;
                    end
                    tline = fgetl(fid);    % this should be $Nodes or $PhysicalNames
                    if (feof(fid))
                        fprintf('Syntax error (no $Nodes or $PhysicalNames) in: %s',  filename);
                        fileformat = 0;
                    end
                else
                    fprintf('Syntax error (no version?) in: %s',  filename);
                    fileformat = 0;
                end
            end
            
            if (~fileformat)
                return
            end
            
            if strcmp(tline, '$PhysicalNames')
                tline = fgetl(fid);
                while ~strcmp(tline, '$EndPhysicalNames')
                    tline = fgetl(fid);
                    if (feof(fid))
                        fprintf('Syntax error (no $EndPhysicalNames) in: %s',  filename);
                        fileformat = 0;
                    end
                end
                if (~fileformat)
                    return
                end
                tline = fgetl(fid); % this should be $Nodes
            end

            %-------------------------------------------------------------------
            % Read nodes            
            if strcmp(tline, '$NOD') || strcmp(tline, '$Nodes')
                obj.n_nodes = fscanf(fid, '%d', 1);
                aux = fscanf(fid, '%g', [4 obj.n_nodes]);
                obj.coords = aux(2:4,:)';
                obj.number_of_node = aux(1,:); % This may not be an identity
                fgetl(fid); % End previous line
                fgetl(fid); % Has to be $ENDNOD $EndNodes
            else
                fprintf('Syntax error (no $Nodes/$NOD) in: %s',  filename);
                fileformat = 0;
            end
            
            if (~fileformat)
                obj.clean();
                return
            end
            
            %-------------------------------------------------------------------
            % Read elements
            tline = fgetl(fid);
            if strcmp(tline,'$ELM') || strcmp(tline, '$Elements')
                obj.n_elem = fscanf(fid, '%d', 1);
                obj.faces_elems = zeros(1, obj.n_elem);
                % read all info about elements into aux (it is faster!)
                aux = fscanf(fid, '%g', inf);
                start = 1;
                obj.ele_infos = zeros(obj.n_elem, 3); % 1 - id, 2 - type, 3 - ntags
                obj.ele_nodes = cell(obj.n_elem,1);
                obj.nodes_verts = [];
                obj.ele_tags = cell(obj.n_elem,1);
                % Defaults for ele_tags
                % format 1: 1 - physical number, 2 - geometrical number
                % format 2: 1 - physical number, 2 - geometrical number, 3 - mesh partition number
                for I = 1:obj.n_elem
                    if (fileformat == 2)
                        finish = start + 2;
                        obj.ele_infos(I, 1:3) = aux(start:finish);
                        ntags = aux(finish);
                        start = finish + 1;
                        finish = start + ntags -1;
                        obj.ele_tags{I} = aux(start:finish);
                        start = finish + 1;
                    else
                        finish = start + 1;
                        obj.ele_infos(I, 1:2) = aux(start:finish);
                        start = finish + 1; 
                        % the third element is ntags, which was fixed
                        obj.ele_infos(I, 3) = 2;
                        finish = start + 1;
                        obj.ele_tags{I} = aux(start:finish);
                        start = finish + 2; 
                    end
                    type = obj.ele_infos(I, 2);
                    nnodes = obj.types(type,2);
                    nverts = obj.types(type,3);
                    if (obj.types(type,1) == 2)
                        obj.nc = obj.nc + 1;
                        obj.faces_elems(I) = obj.nc;
                        finish = start + nverts - 1;
                        % Check use of IDS !!!!!
                        obj.nodes_verts = [ obj.nodes_verts aux(start:finish)'];
                    end
                    finish = start + nnodes - 1;
                    % Check use of IDS !!!!!
                    obj.ele_nodes{I} = aux(start:finish);
                    start=finish + 1;
                end
                fgetl(fid); % Has to be $ENDELM or $EndElements
                obj.nodes_verts = unique(obj.nodes_verts);
            else
                fprintf('Syntax error (no $Elements/$ELM) in: %s',  filename);
                fileformat = 0;
            end
            
            if (~fileformat)
                obj.clean();
                return
            end
            fclose(fid);

            sparseovhd = 2; % The overhead of using a sparse vector
            % 2 is a guess for memory, may be tuned

            % Do the mapping between numbers
            max_node_number = max(obj.number_of_node);
            if (obj.n_nodes*sparseovhd < max_node_number) 
                obj.node_of_number = sparse(1, obj.number_of_node, 1:obj.n_nodes, ...
                    1, max_node_number);
            else
                obj.node_of_number = zeros(1, max_node_number);
                obj.node_of_number(obj.number_of_node) = 1:obj.n_nodes;
            end

            max_elem_number = max(obj.ele_infos(:,1));
            if (obj.n_elem*sparseovhd < max_elem_number) 
                obj.elem_of_number = sparse(1, obj.ele_infos(:,1), 1:obj.n_elem, ...
                    1, max_elem_number);
            else
                obj.elem_of_number = zeros(1, max_elem_number);
                obj.elem_of_number(obj.ele_infos(:,1)) = 1:obj.n_elem;
            end

            
            % Is this really necessary?
            for dim = 0:3
                obj.n_elem_by_dimension(dim+1) = sum(obj.types(obj.ele_infos(:,2),1)==dim);
            end
            
            obj.nv = size(obj.nodes_verts,2);
            % obj.nc = obj.n_elem_by_dimension(3); % 1 is for 0D
            
            % Build the inverse map verts_nodes
            if (obj.nv*sparseovhd < obj.n_nodes) 
                obj.verts_nodes = sparse(1, obj.nodes_verts, 1:obj.nv, 1, obj.n_nodes);
            else
                obj.verts_nodes = zeros(1, obj.n_nodes);
                obj.verts_nodes(obj.nodes_verts) = 1:obj.nv;
            end
            
            % Build the inverse map elems_faces (Always dense?)
            obj.elems_faces = find(obj.faces_elems>0);
            obj.build();
            
            % Build the map (and inverse) elems_edges (Always sparse?)
            obj.edges_elems = sparse(1, obj.n_elem);
            obj.elems_edges = sparse(1, obj.ne);
            
            for elem = find(obj.types(obj.ele_infos(:,2),1) == 1)'
                % Could be simpler? (Pick one element adjacent to the
                % vertex and use its info to track the edge?)
                [~, edges_start] = obj.get_ce_v(obj.verts_nodes(obj.ele_nodes{elem}(1)));
                [~, edges_end] = obj.get_ce_v(obj.verts_nodes(obj.ele_nodes{elem}(2)));
                edge = intersect(edges_start, edges_end);
                obj.elems_edges(edge) = elem;
                obj.edges_elems(elem) = edge;
            end
%             obj.elems_edges = sparse(1, xx, xx);

            % Build the map (and inverse) elems_verts (Always sparse?)
            obj.verts_elems = sparse(1, obj.n_elem);
            obj.elems_verts = sparse(1, obj.nv);
            
            for elem = find(obj.types(obj.ele_infos(:,2),1) == 0)'
                vert = obj.verts_nodes(obj.ele_nodes{elem}(1));
                obj.elems_verts(vert) = elem;
                obj.verts_elems(elem) = vert;
            end

        end
     end
        
    %% Methods that implement abstract methods declared in <che.html *che*> superclass
    methods
        %-----------------------------------------------------------------------
        % Implementation of the abstract method that returns the number of
        % vertices of a face.
        function nvf = get_nv_cell(obj, f_id)
            e_id = obj.elems_faces(f_id);
            type = obj.ele_infos(e_id,2);
            if ( obj.types(type,1) == 2)
                nvf = obj.types(type,3);
            else
                nvf = 0;
            end
        end

        %-----------------------------------------------------------------------
        % Implementation of the abstract method that returns a vertex index
        % given its position k on a given face.
        function v_id = get_v_cell(obj, f_id, k)
            e_id = obj.elems_faces(f_id);
            type = obj.ele_infos(e_id,2);
            if ( obj.types(type,1) == 2)
                % Check valid k?
                % Check number_of_node?
                v_id = obj.verts_nodes(obj.ele_nodes{e_id}(k));
            else
                v_id = 0;
            end
        end
    end
    
end

