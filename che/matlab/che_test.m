%% CHE_TEST subclass of <che.html *che*> class to test it.
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
%
% This is a subclass of <che.html *che*> class to demonstrate its usage.
%
% Please, refer to documentation of <che.html *che*> class.
%
% <che.html *che*> is an abstract class, which contains abstract methods:  
%%%
% * *get_nv_cell*, which returns the number of vertices of a cell.
% * *get_v_cell*, which returns a vertex of a cell.
% 
% This subclass implements these two abstract methods. Therefore, it is
% a concrete class that can be instatiated (i.e., it is possible to create
% objects of this subclass).
%
% Objects of this subclass have two properties:
%%%
% * *cell_nv*: vector of number of vertices of cells (size = *nc*)
% * *cell_vincid*: vector (of matlab cells) with vertex incidence of cells (size = *nc*)
%
% Public method *load_cell* is used to load the number of vertices and the
% vertex list of a cell (given by its index to vectors *cell_nv* and
% *cell_vincid*).
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Class definition
classdef che_test < che

    %% Public properties
    properties
        cell_nv       = [];  % vector with number of vertices of cells (size = "nc")
        cell_vincid   = {};  % vector (of matlab cells) with vertex incidence of cells (size = "nc")
    end
    
    %% Constructor method
    methods
        %-----------------------------------------------------------------------
        % Constructor method.
        function obj = che_test(num_v, num_c)
            if (nargin == 0)
                num_v = 0;
                num_c = 0;
            end
            obj = obj@che(num_v, num_c);  % using superclass constructor with arguments
            
            % Pre-allocate this subclass vector properties
            obj.cell_nv = zeros(num_c,1);
            obj.cell_vincid = cell(num_c,1);
        end
    end
        
    %% Public method to load number of vertices and vertex list of a cell
    methods
        %-----------------------------------------------------------------------
        % Loads the number of vertices and the vertex list of a given cell.
        function load_cell(obj, c_id, nv_c, c_vincid)
            if (obj.nv==0)
                return;
            end
            if (obj.nc==0)
                return;
            end

            if (size(c_vincid,2) ~= nv_c)
                fprintf('Error: cell with %d vertices, instead of %d!\n', ...
                        size(c_vincid,2), nv_c);
                return;
            else
                obj.cell_nv(c_id) = nv_c;
                obj.cell_vincid{c_id} = c_vincid;
            end
        end
     end
        
    %% Methods that implements abstract methods declared in <che.html *che*> superclass
    methods
        %-----------------------------------------------------------------------
        % Implementation of abstract method that returns the number of
        % vertices of a face.
        function nv_cell = get_nv_cell(obj, c_id)
            nv_cell = obj.cell_nv(c_id);  
        end

        %-----------------------------------------------------------------------
        % Implementation of abstract method that a vertex index given its
        % position k on a given face.
        function v_cell = get_v_cell(obj, c_id, k)
            v_cell = obj.cell_vincid{c_id}(k);
        end
    end
end