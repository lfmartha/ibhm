# IBHM

MatLab files and documentation for the IBHM (Index-based hybrid mesh) data structures described in DOI: 10.1007/s00366-015-0395-0

## Developed by:
* JP Moitinho de Almeida (moitinho@civil.ist.utl.pt) - Instituto Superior Técnico - Universidade de Lisboa (https://tecnico.ulisboa.pt/en/)
* Luiz Fernando Martha (lfm@tecgraf.puc-rio.br) - Pontifícia Universidade Católica do Rio de Janeiro - PUC-Rio (www.puc-rio.br)

## Quick start:

### For che, the 2D data structure 

* If using the gmsh interface, copy che.m and che_gmsh.m to your working directory and execute

```matlab
     m = che_gmsh(); 
     m.load('yourfile.msh'); % a sample 'Simple.msh' is provided
     %  you can now access the adjacency information, for example of vertex 5
     [faces, edges] = m.get_ce_v(5);
```

* A "bare" implementation is also available in che_test.m, with a test call in run_che_test.m

### For chf, the 3D data structure

* If using the gmsh interface, copy chf.m, chf_cell.m and che_gmsh.m to your working directory and execute

```matlab
     m = chf_gmsh(); 
     m.load('yourfile.msh'); % a sample 'Tap1.msh' is provided
     %  you can now access the adjacency information, for example of vertex 5
     [cells, faces, edges] = m.get_cfe_v(5);
```

* A "bare" implementation is also available in chf_test.m, with several test calls in run_chf_*.m

## Wiki

Check the [wiki](https://gitlab.com/lfmartha/ibhm/wikis/home) for more details.

## Matlab documentation

* The documentation, as published in matlab, is available in folders che/doc and chf/doc.
