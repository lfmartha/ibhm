%% CHF_TEST subclass of <chf.html *chf*> class to test it.
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
%
% This is a subclass of <chf.html *chf*> class to demonstrate its usage.
%
% Please, refer to documentation of <chf.html *chf*> class.
%
% <chf.html *chf*> is an abstract class that contains one abstract method:
%%%
% * *get_cell*, which returns, for a given cell index, its type (a type of
% cell defined by client at registration) and a vertex incidence list of a
% solid cell.
%
% This subclass implements this method. Therefore, it is a concrete class
% that can be instatiated (i.e., it is possible to create objects of this
% subclass).
%
%
% Objects of this subclass have three properties:
%%%
% * *sm_dimensioned*: flag for already dimensioned solid mesh input data
% * *c_type*: vector of cell types (size = *nc*, in which a cell type is a
% number defined by client that identifies the topology of a cell)
% * *c_vincid*: matrix with vertex incidence of cells (size = *nc* x *max_nv_c*,
% in which *max_nv_c* is a property of superclass <chf.html *chf*> with
% the maximum number of vertices of all solid cells)
%
% The topology of a cell in relation to its local vertex incidence list is
% registered by the client using method *register_cell_templ* of superclass
% <chf.html *chf*>.
%
% Public method *load_cell* is used to load the type and the vertex list of
% a cell (given by its index to vector *c_type* and to matrix *c_vincid*).
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Class definition
classdef chf_test < chf

    %% Protected properties (visible in subclasses)
    properties (Access = protected)
        sm_dimensioned = 0; % flag for already dimensioned solid mesh input data
        c_type = [];        % vector of cell types (size = "nc")
        c_vincid = [];      % matrix with vertex incidence of cells (size = "nc" x "max_nv_c")
    end

    %% Constructor and clean methods
    methods
        %-----------------------------------------------------------------------
        % Constructor method.
        % Initializes number of vertices and number of solid cells
        % based on given values.
        function obj = chf_test(num_v, num_c)
            obj = obj@chf(num_v, num_c);
        end

        %-----------------------------------------------------------------------
        % Cleans mesh data structure of an CHF object.
        function obj = clean(obj)
            clean@chf(obj);
            obj.sm_dimensioned = 0;
            obj.c_type = [];
            obj.c_vincid = [];
        end
    end
        
    %% Public method to load type and vertex list of a cell
    methods
        %-----------------------------------------------------------------------
        % Loads the type and the vertex list of of a given solid cell.
        % It is assumed that client has registered all cell templates using
        % method register_cell_templ of superclass CHF.
        function load_cell(obj, c_id, type, cell_incid)
            if (obj.nv==0)
                return;
            end
            if (obj.nc==0)
                return;
            end

            if (obj.sm_dimensioned == 0)
                % Dimension vector of solid cell types and matrix with cell
                % vertices
                obj.c_type = zeros(1, obj.nc);
                obj.c_vincid = zeros(obj.nc, obj.max_nv_c);
                obj.sm_dimensioned = 1;
            end

            obj.c_type(c_id) = type;
            templ = obj.cell_templ{obj.templ_index(type)};
            nv_c = templ.nv;
            if (size(cell_incid,2) ~= nv_c)
                    fprintf('Invalid: Type %d cell, with %d vertices, instead of %d!\n', ...
                        type, size(cell_incid,2), nv_c);
                return
            else
                obj.c_vincid(c_id,1:nv_c) = cell_incid;
            end
        end
     end
        
    %% Method that implements abstract method declared in <chf.html *chf*> superclass
    methods
        %-----------------------------------------------------------------------
        % Implementation of the abstract method declared in superclass CHF.
        % Returns the type and the vertex list of of a given solid cell.
        function [ type, verts ] = get_cell(obj, c_id)
            if (obj.nv==0 || obj.nc==0)
                return;
            end
            type = obj.c_type(c_id);
            verts = obj.c_vincid(c_id,:);
        end
    end
end