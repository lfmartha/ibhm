%% run_chf_5cells.m
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% Using <chf_test.html *chf_test*>, this code creates a simple mesh (with
% five cells: three hexahedra, one wedge, and one tetrahedron), and prints
% adjacency information based on cells, faces, edges, and vertices.
%
% Mesh description in file <chf_test-5cells.pdf>
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Mesh creation using <chf_test.html *chf_test*> with 5 cells
clear
clc

m = chf_test(17,5);

m.register_cell_templ(1, 4, 4, [2 3 4; 3 1 4; 4 1 2; 1 3 2]);
m.register_cell_templ(2, 8, 6, [1 4 3 2; 5 6 7 8; 1 5 8 4; 2 3 7 6; 1 2 6 5; 4 8 7 3]);
m.register_cell_templ(3, 6, 5, [1 3 2 0; 4 5 6 0; 1 2 5 4; 2 3 6 5; 3 1 4 6]);

type = 2;
verts = [1 2 4 3 9 10 12 11];
m.load_cell(1,type, verts);

type = 3;
verts = [2 5 4 10 13 12];
m.load_cell(2,type, verts);

type = 2;
verts = [3 4 7 6 11 12 15 14];
m.load_cell(3,type, verts);

type = 2;
verts = [12 4 7 15 13 5 8 16];
m.load_cell(4,type, verts);

type = 1;
verts = [13 10 17 12];
m.load_cell(5,type, verts);

%% Construction of <chf.html *chf*> mesh resprentation
build(m);

%% Print mesh adjacency information using function <chf_dump.html *chf_dump*>
chf_dump(m);

%% Clean-up memory
clean(m);
delete(m);