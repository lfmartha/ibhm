%% run_chf_OneHex.m
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% Using <chf_test.html *chf_test*>, this code creates a mesh with only one
% hexahedron cell, and prints adjacency information based on cells, faces,
% edges, and vertices.
%
% Mesh description in file <chf_test-OneHex.pdf>
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Mesh creation using <chf_test.html *chf_test*> with one hexahedron
clear
clc

m = chf_test(8,1);

m.register_cell_templ(1, 8, 6, [1 4 3 2; 5 6 7 8; 1 5 8 4; 2 3 7 6; 1 2 6 5; 4 8 7 3]);

type = 1;
verts = [1 2 3 4 5 6 7 8];
m.load_cell(1,type, verts);

%% Construction of <chf.html *chf*> mesh resprentation
build(m);

%% Print mesh adjacency information using function <chf_dump.html *chf_dump*>
chf_dump(m);

%% Clean-up memory
clean(m);
delete(m);