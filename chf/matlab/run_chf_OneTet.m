%% run_chf_OneTet.m
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% Using <chf_test.html *chf_test*>, this code creates a mesh with only one
% tetrahedron cell, and prints adjacency information based on cells, faces,
% edges, and vertices.
%
% Mesh description in file <chf_test-OneTet.pdf>
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Mesh creation using <chf_test.html *chf_test*> with one tetrahedron
clear
clc

m = chf_test(4,1);

m.register_cell_templ(1, 4, 4, [2 3 4; 3 1 4; 4 1 2; 1 3 2]);

type = 1;
verts = [1 2 3 4];
m.load_cell(1,type, verts);

%% Construction of <chf.html *chf*> mesh resprentation
build(m);

%% Print mesh adjacency information using function <chf_dump.html *chf_dump*>
chf_dump(m);

%% Clean-up memory
clean(m);
delete(m);