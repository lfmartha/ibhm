%% run_chf_OneWedge.m
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% Using <chf_test.html *chf_test*>, this code creates a mesh with only one
% wedge cell, and prints adjacency information based on cells, faces,
% edges, and vertices.
%
% Mesh description in file <chf_test-OneWedge.pdf>
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
%% Mesh creation using <chf_test.html *chf_test*> with one wedge
clear
clc

m = chf_test(6,1);

m.register_cell_templ(1, 6, 5, [1 3 2 0; 4 5 6 0; 1 2 5 4; 2 3 6 5; 3 1 4 6]);

type = 1;
verts = [1 2 3 4 5 6];
m.load_cell(1,type, verts);

%% Construction of <chf.html *chf*> mesh resprentation
build(m);

%% Print mesh adjacency information using function <chf_dump.html *chf_dump*>
chf_dump(m);

%% Clean-up memory
clean(m);
delete(m);