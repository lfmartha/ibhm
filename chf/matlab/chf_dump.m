%% chf_dump.m
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% For a <chf.html *chf*> mesh object in memory, this function prints adjacency
% information based on cells, faces, edges, and vertices.
%
%% History
%
% @VERSION 1.00 on 30-Jul-2019. Initial version.
%
function [] = chf_dump(m)
%% Mesh adjacency information based on cells
fprintf(1,'\n');
fprintf(1,'Vertices, edges, and faces of cells:\n');
fprintf(1,'\n');
for c_id = 1:m.nc
    [faces, edges, vertices] = get_fev_c(m, c_id);
    nfaces = size(faces,2);
    nedges = size(edges,2);
    nverts = size(vertices,2);

    fprintf(1,'Cell: %2d\n', c_id);

    for k = 1:nverts
        fprintf(1,'v%d: %2d\t', k, vertices(k));
    end
    fprintf(1,'\n');
    for k = 1:nedges
        fprintf(1,'e%d: %2d\t', k, edges(k));
    end
    fprintf(1,'\n');
    for k = 1:nfaces
        fprintf(1,'f%d: %2d\t', k, faces(k));
    end
    fprintf(1,'\n');
    fprintf(1,'\n');
end

%% Mesh adjacency information based on faces
fprintf(1,'\n');
fprintf(1,'Vertices, edges, and cells of faces:\n');
fprintf(1,'\n');
for f_id = 1:m.nf
    [cells, edges, vertices] = get_cev_f(m, f_id);
    ncells = size(cells,2);
    nedges = size(edges,2);
    nverts = size(vertices,2);

    fprintf(1,'Face: %2d\n', f_id);

    for k = 1:nverts
        fprintf(1,'v%d: %2d\t', k, vertices(k));
    end
    fprintf(1,'\n');
    for k = 1:nedges
        fprintf(1,'e%d: %2d\t', k, edges(k));
    end
    fprintf(1,'\n');
    for k = 1:ncells
        fprintf(1,'c%d: %2d\t', k, cells(k));
    end
    fprintf(1,'\n');
    fprintf(1,'\n');
end

%% Mesh adjacency information based on edges
fprintf(1,'\n');
fprintf(1,'Vertices, faces, and cells of edges:\n');
fprintf(1,'\n');
for e_id = 1:m.ne
    [cells, faces, vertices] = get_cfv_e(m, e_id);
    ncells = size(cells,2);
    nfaces = size(faces,2);

    fprintf(1,'Edge: %2d\n', e_id);

    fprintf(1,'v1: %2d\tv2: %2d', vertices(1), vertices(2));
    fprintf(1,'\n');
    for k = 1:nfaces
        fprintf(1,'f%d: %2d\t', k, faces(k));
    end
    fprintf(1,'\n');
    for k = 1:ncells
        fprintf(1,'c%d: %2d\t', k, cells(k));
    end
    fprintf(1,'\n');
    fprintf(1,'\n');
end

%% Mesh adjacency information based on vertices
fprintf(1,'\n');
fprintf(1,'Edges, faces, and cells of vertices:\n');
fprintf(1,'\n');
for v_id = 1:m.nv
    [cells, faces, edges] = get_cfe_v(m, v_id);
    ncells = size(cells,2);
    nfaces = size(faces,2);
    nedges = size(edges,2);

    fprintf(1,'Vertex: %2d\n', v_id);

    for k = 1:nedges
        fprintf(1,'e%d: %2d\t', k, edges(k));
    end
    fprintf(1,'\n');
    for k = 1:nfaces
        fprintf(1,'f%d: %2d\t', k, faces(k));
    end
    fprintf(1,'\n');
    for k = 1:ncells
        fprintf(1,'c%d: %2d\t', k, cells(k));
    end
    fprintf(1,'\n');
    fprintf(1,'\n');
end
end